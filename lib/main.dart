
import 'package:compra_coletiva/ui/login/login_page.dart';
import 'package:compra_coletiva/ui/login/new_login_page.dart';
import 'package:flutter/material.dart';

import 'ui/home/home_page.dart';

void main() {
	runApp(
		new MaterialApp(
			debugShowCheckedModeBanner: false,
			title: "SideBuySide - Seu app de compras coletivas!",
			home: NewLoginPage(),
		),
	);
}
