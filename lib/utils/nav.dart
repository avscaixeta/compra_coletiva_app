import 'package:flutter/material.dart';

push(BuildContext context, Widget page) {
	Navigator.push(context, MaterialPageRoute(builder: (context) {
		return page;
	}));
}

pushReplacement(BuildContext context, Widget page) {
	// O pushReplacement deixa de empilhar as páginas, com isso o botão voltar desaperece da página seguinte
	Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
		return page;
	}));
}
