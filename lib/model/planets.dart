class Planet {
  final String id;
  final String name;
  final String location;
  final String distance;
  final String gravity;
  final String description;
  final String image;
  final String picture;

  const Planet({this.id, this.name, this.location, this.distance, this.gravity,
    this.description, this.image, this.picture});
}

List<Planet> planets = [
  const Planet(
    id: "1",
    name: "Grupo 01",
    location: "Administradora Maria",
    distance: "(61)99828-0155",
    gravity: "value",
    description: "Mars is the fourth planet from the Sun and the second-smallest planet in the Solar System after Mercury. In English, Mars carries a name of the Roman god of war, and is often referred to as the 'Red Planet' because the reddish iron oxide prevalent on its surface gives it a reddish appearance that is distinctive among the astronomical bodies visible to the naked eye. Mars is a terrestrial planet with a thin atmosphere, having surface features reminiscent both of the impact craters of the Moon and the valleys, deserts, and polar ice caps of Earth.",
    image: "assets/img/grupo01.png",
    picture: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSxgqVR1vH4sRyE4Hu3g5PoKc4jWe-8qypO6Kb3xkofz4yPFTd"
  ),
  const Planet(
    id: "2",
    name: "Grupo 02",
    location: "Administradora Josefa",
    distance: "(61) 99828-0155",
    gravity: "3.711 m/s ",
    description: "Neptune is the eighth and farthest known planet from the Sun in the Solar System. In the Solar System, it is the fourth-largest planet by diameter, the third-most-massive planet, and the densest giant planet. Neptune is 17 times the mass of Earth and is slightly more massive than its near-twin Uranus, which is 15 times the mass of Earth and slightly larger than Neptune. Neptune orbits the Sun once every 164.8 years at an average distance of 30.1 astronomical units (4.50×109 km). It is named after the Roman god of the sea and has the astronomical symbol ♆, a stylised version of the god Neptune's trident",
    image: "assets/img/grupo02.png",
    picture: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSxgqVR1vH4sRyE4Hu3g5PoKc4jWe-8qypO6Kb3xkofz4yPFTd"
  ),
  const Planet(
    id: "3",
    name: "Grupo 03",
    location: "Administradora Filó",
    distance: "(61) 99828-0155",
    gravity: "3.711 m/s ",
    description: "The Moon is an astronomical body that orbits planet Earth, being Earth's only permanent natural satellite. It is the fifth-largest natural satellite in the Solar System, and the largest among planetary satellites relative to the size of the planet that it orbits (its primary). Following Jupiter's satellite Io, the Moon is second-densest satellite among those whose densities are known.",
    image: "assets/img/grupo03.png",
    picture: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSxgqVR1vH4sRyE4Hu3g5PoKc4jWe-8qypO6Kb3xkofz4yPFTd"
  ),
  const Planet(
    id: "4",
    name: "Earth",
    location: "Milkyway Galaxy",
    distance: "54.6m Km",
    gravity: "3.711 m/s ",
    description: "Earth is the third planet from the Sun and the only object in the Universe known to harbor life. According to radiometric dating and other sources of evidence, Earth formed over 4 billion years ago. Earth's gravity interacts with other objects in space, especially the Sun and the Moon, Earth's only natural satellite. Earth revolves around the Sun in 365.26 days, a period known as an Earth year. During this time, Earth rotates about its axis about 366.26 times.",
    image: "assets/img/earth.png",
    picture: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSxgqVR1vH4sRyE4Hu3g5PoKc4jWe-8qypO6Kb3xkofz4yPFTd"
  ),
  const Planet(
    id: "5",
    name: "Mercury",
    location: "Milkyway Galaxy",
    distance: "54.6m Km",
    gravity: "3.711 m/s ",
    description: "Mercury is the smallest and innermost planet in the Solar System. Its orbital period around the Sun of 88 days is the shortest of all the  in the Solar System. It is named after the Roman deity Mercury, the messenger to the gods.",
    image: "assets/img/mercury.png",
    picture: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSxgqVR1vH4sRyE4Hu3g5PoKc4jWe-8qypO6Kb3xkofz4yPFTd"
  ),
];