class Response {
	final String status;
	final String access_token;

  	Response(this.status, this.access_token);

  	Response.fromJson(Map<String, dynamic> map) :
			status = map["status"],
			access_token = map["access_token"]
	;

}