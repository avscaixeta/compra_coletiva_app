class User {
	int id;
	String name;
	String email;
	String cellphone;
	String cpf;
	String gender;
	String birthday;
	int active;
	int fileId;

	User({
		this.id, this.name, this.email, this.cellphone, this.cpf, this.gender, this.birthday, this.active, this.fileId
	});

	factory User.fromJson(Map<String, dynamic> json) {
		return new User(
			id: json["id"],
			name: json["name"],
			email: json["email"],
			cellphone: json["cellphone"],
			cpf: json["cpf"],
			gender: json["gender"],
			birthday: json["birthday"],
			active: json["active"],
			fileId: json["file_id"],
//			data: json['data'].map((value) => new OrderData.fromJson(value)).toList()
		);
	}
}
//
//class OrderData {
//	int id;
//	String name;
//	String email;
//	String cellphone;
//	String cpf;
//	String gender;
//	String birthday;
//	int active;
//	int file_id;
//
//	OrderData({
//		this.id, this.name, this.email, this.cellphone, this.cpf, this.gender, this.birthday, this.active, this.file_id
//	});
//
//	factory OrderData.fromJson(Map<String, dynamic> json) {
//		return new OrderData(
//			id: json['id'],
//			name: json['name'],
//			description: json['description'],
//			userId: json['user_id'],
//			active: json['active'],
//			fileId: json['file_id'],
//			imageId: json['image_id'],
//		);
//	}
//}




/*

{
    "status": "success",
    "data": {
        "id": 1,
        "name": "Teste",
        "description": "Descrição teste",
        "active": 1,
        "store_id": 1,
        "order_type_id": 1,
        "order_history_id": 1,
        "created_at": null,
        "updated_at": null,
        "file_id": 30,
        "order_history": {
            "id": 1,
            "observation": "Aguardando catálogo do fornecedor",
            "active": 1,
            "status_limit_date": null,
            "tracking_code": null,
            "order_id": 1,
            "order_status_id": 1,
            "created_at": null,
            "updated_at": null,
            "status": {
                "id": 1,
                "name": "Em preparação",
                "description": "O pedido ainda não está aberto para compras, o vendedor está preparando o pedido.",
                "active": 1,
                "created_at": "2018-08-30 13:08:59",
                "updated_at": "2018-08-30 13:08:59"
            }
        },
        "image": {
            "id": 30,
            "name": "banco3.jpg",
            "unique_name": "1d19011888c3b78217a78f737cf51b98.jpg",
            "path": "stores/1/1d19011888c3b78217a78f737cf51b98.jpg",
            "bucket_amazon": "compras-coletivas",
            "active": 1,
            "created_at": "2019-07-19 16:51:07",
            "updated_at": "2019-07-19 16:51:07",
            "user_id": 1,
            "file_type_id": 1,
            "url": "https://compras-coletivas.s3.sa-east-1.amazonaws.com/stores/1/1d19011888c3b78217a78f737cf51b98.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIARX57JGLSNSH2B4U7%2F20190722%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190722T162244Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Signature=3fe5a6c03fe29039afe6a408bd739e36247e7831e1c4aeccb5918decb86f8efb",
            "url_thumb": "https://compras-coletivas.s3.sa-east-1.amazonaws.com/stores/1/thumb/1d19011888c3b78217a78f737cf51b98.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIARX57JGLSNSH2B4U7%2F20190722%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190722T162244Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Signature=28d47ed53914dffccaf686a441f8d7c249a7b22a7969965518992d77f78c4017"
        }
    }
}
 */

