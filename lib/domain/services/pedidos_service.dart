import 'package:compra_coletiva/domain/pedidos.dart';

class PedidosService {

	static List<Pedidos> getPedidos() {

		List<Pedidos> listaPedidos = new List<Pedidos>();

		String nomeItem1 = "UP Baby Spring Colletion 19";
		String nomeItem2 = "UP Child Summer Colletion 19";
		String nomeItem3 = "UP Adult Autumn Colletion 19";
		String nomeItem4 = "UP Baby Winter Colletion 19";
		String nomeItem5 = "teste";

		String dataAbertura1 = "12/07/2019";
		String dataAbertura2 = "15/07/2019";
		String dataAbertura3 = "17/07/2019";
		String dataAbertura4 = "20/07/2019";
		String dataAbertura5 = "22/07/2019";

		String dataFechamento1 = "31/07/2019";
		String dataFechamento2 = "04/08/2019";
		String dataFechamento3 = "05/08/2019";
		String dataFechamento4 = "17/08/2019";
		String dataFechamento5 = "31/07/2019";

		String endImagem1 = "assets/img/sapato.jpg";
		String endImagem2 = "assets/img/sapato.jpg";
		String endImagem3 = "assets/img/sapato.jpg";
		String endImagem4 = "assets/img/sapato.jpg";
		String endImagem5 = "assets/img/sapato.jpg";

		listaPedidos.add(Pedidos(nomeItem1, dataAbertura1, dataFechamento1, endImagem1));
		listaPedidos.add(Pedidos(nomeItem2, dataAbertura2, dataFechamento2, endImagem2));
		listaPedidos.add(Pedidos(nomeItem3, dataAbertura3, dataFechamento3, endImagem3));
		listaPedidos.add(Pedidos(nomeItem4, dataAbertura4, dataFechamento4, endImagem4));
		listaPedidos.add(Pedidos(nomeItem5, dataAbertura5, dataFechamento5, endImagem5));

		return listaPedidos;
	}

}