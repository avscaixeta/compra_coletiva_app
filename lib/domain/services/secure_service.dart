import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SecureService extends StatefulWidget {
	@override
	_SecureServiceState createState() => _SecureServiceState();

	static final _storage = new FlutterSecureStorage();

	static Future<List<String>> readAll() async {
		final all = await _storage.readAll();
		return all.keys.map((key) => all[key]).toList(growable: false);
	}

	static Future<String> read() async {
		final token = await _storage.read(key: "key");
		return token;
	}

	static void deleteAll() async {
		await _storage.deleteAll();
		readAll();
	}

	static void addNewItem(token) async {
		final String key = "key";
		final String value = token;

		await _storage.write(key: key, value: value);
		readAll();
	}

}

class _SecureServiceState extends State<SecureService> {
	@override
	Widget build(BuildContext context) {
		// TODO: implement build
		return null;
	}
}
