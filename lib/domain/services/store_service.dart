import 'dart:convert';
import 'package:compra_coletiva/domain/store.dart';
import 'package:compra_coletiva/domain/services/secure_service.dart';
import 'package:http/http.dart' as http;

class StoreService {

	static Future<Store> getStore() async{
		final secureToken = await SecureService.read();

		var url = "http://api.sidebuyside.com.br/store/my/list";

		final response = await http.get(url, headers: {'Authorization' : 'Bearer $secureToken'});
		final Map<String, dynamic> parsed = json.decode(response.body);
		
		return Store.fromJson(parsed);
	}

}