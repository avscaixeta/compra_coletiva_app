import 'dart:convert';
import 'package:compra_coletiva/domain/response.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

class LoginService {

	static final _storage = new FlutterSecureStorage();

	static Future<Response> login(String email, String password) async{
		var url = "http://api.sidebuyside.com.br/oauth/token";

		final response = await http.post(
			url,
			body:
			{
				'email': email,
				'password': password,
				'grant_type': 'password',
				'client_id':'3',
				'client_secret':'LYWBxKEKrfjcjfTNDXRNIroUzw20Q3rf2Il2t6iW',
				'username': email,
				'password': password
			},
//			headers: {'Content-Type' : 'application/json'}
		);
		final Map<String, dynamic> map = json.decode(response.body);

		//Parseando o JSON individualmente
//		String status = map["status"];
//		String access_token = map["access_token"];
//		final r = Response(status, access_token);

		//Parseando o json com uma lista de inicialização que está na classe Response
		final r = Response.fromJson(map);

		return r;
	}

	static Future<List<String>> readAll() async {
		final all = await _storage.readAll();
		return all.keys
				.map((key) => all[key])
				.toList(growable: false);

	}
}