import 'dart:convert';
import 'package:compra_coletiva/domain/order.dart';
import 'package:compra_coletiva/domain/store.dart';
import 'package:compra_coletiva/domain/services/secure_service.dart';
import 'package:http/http.dart' as http;

class OrderService {

	static Future<Order> getOrder() async{
		final secureToken = await SecureService.read();

		String storeId = "2";

		var url = "http://api.sidebuyside.com.br/orders?with=orderHistory,orderHistory.status&store_id=${storeId}";

		final response = await http.get(url, headers: {'Authorization' : 'Bearer $secureToken'});
		final Map<String, dynamic> parsed = json.decode(response.body);
		
		return Order.fromJson(parsed);
	}

}