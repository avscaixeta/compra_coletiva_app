import 'dart:convert';
import 'package:compra_coletiva/domain/services/secure_service.dart';
import 'package:compra_coletiva/domain/user.dart';
import 'package:http/http.dart' as http;

class UserService {

	static Future<User> getUser() async{
		final secureToken = await SecureService.read();

		var url = "api.sidebuyside.com.br/user/get-identity";

		final response = await http.get(url, headers: {'Authorization' : 'Bearer $secureToken'});
		final Map<String, dynamic> map = json.decode(response.body);

		return User.fromJson(map);
	}
}