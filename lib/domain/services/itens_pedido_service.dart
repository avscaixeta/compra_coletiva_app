
import 'package:compra_coletiva/domain/itens_pedido.dart';

class ItensPedidoService {

	static List<ItensPedido> getItensPedido() {

		List<ItensPedido> listaItensPedido = new List<ItensPedido>();

		String nomeItem1 = "Sapato MAC Bebê";
		String nomeItem2 = "Vestido Rosa Criança";
		String nomeItem3 = "Calça Jeans Masculina";
		String nomeItem4 = "Brinquedo ToyStore";
		String nomeItem5 = "Sandália Havaiana Xuxa";

		String cod1 = "1502";
		String cod2 = "1369";
		String cod3 = "1289";
		String cod4 = "1374";
		String cod5 = "1212";

		String tam1 = "M";
		String tam2 = "G";
		String tam3 = "1";
		String tam4 = "2";
		String tam5 = "3";

		String cor1 = "verde";
		String cor2 = "azul";
		String cor3 = "rosa";
		String cor4 = "branco";
		String cor5 = "amarelo";

		String qtde1 = "1";
		String qtde2 = "2";
		String qtde3 = "1";
		String qtde4 = "3";
		String qtde5 = "1";

		listaItensPedido.add(ItensPedido(nomeItem1, cod1, tam1, cor1, qtde1));
		listaItensPedido.add(ItensPedido(nomeItem2, cod2, tam2, cor2, qtde2));
		listaItensPedido.add(ItensPedido(nomeItem3, cod3, tam3, cor3, qtde3));
		listaItensPedido.add(ItensPedido(nomeItem4, cod4, tam4, cor4, qtde4));
		listaItensPedido.add(ItensPedido(nomeItem5, cod5, tam5, cor5, qtde5));

		return listaItensPedido;

	}

}