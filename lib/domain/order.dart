import 'package:json_annotation/json_annotation.dart';

class Order {
	String status;
	OrderItems orderItems;

	Order({
		this.status,
		this.orderItems,
	});

	factory Order.fromJson(Map<String, dynamic> json) {
		return Order(
			status: json["status"],
			orderItems: OrderItems.createOrderItems(json['data'])
		);
	}
}

class OrderItems {

	int currentPage;
	List<dynamic> orderData;
	String firstPage;
	int from;
	int lastPage;
	String lastPageUrl;
	String nextPageUrl;
	String path;
	int perPage;
	String prevPageUrl;
	int to;
	int total;

	OrderItems(
		{
			this.currentPage,
			this.orderData,
			this.firstPage,
			this.from,
			this.lastPage,
			this.lastPageUrl,
			this.nextPageUrl,
			this.path,
			this.perPage,
			this.prevPageUrl,
			this.to,
			this.total,
		}
	);

	static OrderItems createOrderItems(Map<String, dynamic> json) => OrderItems(
		currentPage: json['current_page'],
		orderData: json['data'].map((value) => OrderData.fromJson(value)).toList(),
		firstPage: json['first_page_url'],
		from: json['from'],
		lastPage: json['last_page'],
		lastPageUrl: json['last_page_url'],
		nextPageUrl: json['next_page_url'],
		path: json['path'],
		perPage: json['per_page'],
		prevPageUrl: json['prev_page_url'],
		to: json['to'],
		total: json['total']
	);

}

@JsonSerializable(nullable: false)
class OrderData {

	int id;
	String name;
	String description;
	int active;
	int storeId;
	int orderTypeId;
	int orderHistoryId;
	int fileId;
	OrderHistory orderHistory;

	OrderData(
		{this.id,
			this.name,
			this.description,
			this.active,
			this.storeId,
			this.orderTypeId,
			this.orderHistoryId,
			this.fileId,
			this.orderHistory,
		});

	factory OrderData.fromJson(Map<String, dynamic> json) {
		return OrderData(
			id: json['id'],
			name: json['name'],
			description: json['description'],
			active: json['active'],
			storeId: json['store_id'],
			orderTypeId: json['order_type_id'],
			orderHistoryId: json['order_history_id'],
			fileId: json['file_id'],
			orderHistory: OrderHistory.createOrderHistory(json['order_history']),
		);
	}
}

class OrderHistory {

	int id;
	String observation;
	int active;
	String statusLimitDate;
	int trackingCode;
	int orderId;
	int orderStatusId;
	StatusOrderHistory statusOrderHistory;

	OrderHistory(
		{	this.id,
			this.observation,
			this.active,
			this.statusLimitDate,
			this.trackingCode,
			this.orderId,
			this.orderStatusId,
			this.statusOrderHistory,
		});

	static OrderHistory createOrderHistory(Map<String, dynamic> json) => OrderHistory(
		id: json['id'],
		observation: json['observation'],
		active: json['active'],
		statusLimitDate: json['status_limit_date'],
		trackingCode: json['tracking_code'],
		orderId: json['order_id'],
		orderStatusId: json['order_status_id'],
		statusOrderHistory: StatusOrderHistory.createStatusOrderHistory(json['status']),
	);
}

class StatusOrderHistory {

	int id;
	String name;
	String description;
	int active;

	StatusOrderHistory(
		{	this.id,
			this.name,
			this.description,
			this.active,
		});

	static StatusOrderHistory createStatusOrderHistory(Map<String, dynamic> json) => StatusOrderHistory(
		id: json['id'],
		name: json['name'],
		active: json['active'],
		description: json['description'],
	);
}
