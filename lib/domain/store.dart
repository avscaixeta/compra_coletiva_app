import 'package:json_annotation/json_annotation.dart';

class Store {
	String status;
	List<dynamic> data;

	Store({
		this.status,
		this.data,
	});

	factory Store.fromJson(Map<String, dynamic> json) {
		return Store(
			status: json["status"],
			data: json['data'].map((value) => StoreData.fromJson(value)).toList());
	}
}

class StoreData {
	int id;
	String name;
	String description;
	int userId;
	int active;
	int fileId;
	int imageId;
	User user;
	ImageStore imageStore;
	//"created_at": "2019-07-23 19:25:23",
	//"updated_at": "2019-07-23 19:25:23",

	StoreData(
		{this.id,
			this.name,
			this.description,
			this.userId,
			this.active,
			this.fileId,
			this.imageId,
			this.user,
			this.imageStore});

	factory StoreData.fromJson(Map<String, dynamic> json) {
		return StoreData(
			id: json['id'],
			name: json['name'],
			description: json['description'],
			userId: json['user_id'],
			active: json['active'],
			fileId: json['file_id'],
			imageId: json['image_id'],
			user: createUser(json['user']),
			imageStore: createImageStore(json['image']));
	}

	static User createUser(data) {
		User user = User(
			id: data['id'],
			name: data['name'],
			email: data['email'],
			cellphone: data['cellphone'],
			cpf: data['cpf'],
			gender: data['gender'],
			birthday: data['birthday'],
			active: data['active'],
			fileId: data['file_id'],
		);
		return user;
	}

	static ImageStore createImageStore(data) {
		ImageStore image = ImageStore(
			id: data['id'],
			name: data['name'],
			uniqueName: data['unique_name'],
			path: data['path'],
			bucketAmazon: data['bucket_amazon'],
			active: data['active'],
			userId: data['user_id'],
			fileTypeId: data['file_type_id'],
			url: data['url'],
			urlThumb: data['url_thumb'],
		);
		return image;
	}
}

@JsonSerializable(nullable: false)
class User {
	int id;
	String name;
	String email;
	String cellphone;
	String cpf;
	String gender;
	int birthday;
	int active;
	int fileId;

	//"created_at": "2019-07-23 19:25:23",
	//"updated_at": "2019-07-23 19:25:23",

	User(
		{this.id,
			this.name,
			this.email,
			this.cellphone,
			this.cpf,
			this.gender,
			this.birthday,
			this.active,
			this.fileId});

	factory User.fromJson(Map<dynamic, dynamic> json) {
		return User(
			id: json['id'],
			name: json['name'],
			email: json['email'],
			cellphone: json['cellphone'],
			cpf: json['cpf'],
			gender: json['gender'],
			birthday: json['birthday'],
			active: json['active'],
			fileId: json['file_id'],
		);
	}
}


@JsonSerializable(nullable: false)
class ImageStore {
	int id;
	String name;
	String uniqueName;
	String path;
	String bucketAmazon;
	int active;
	int userId;
	int fileTypeId;
	String url;
	String urlThumb;
	//"created_at": "2019-07-23 19:25:23",
	//"updated_at": "2019-07-23 19:25:23",

	ImageStore(
		{
			this.id,
			this.name,
			this.uniqueName,
			this.path,
			this.bucketAmazon,
			this.active,
			this.userId,
			this.fileTypeId,
			this.url,
			this.urlThumb
		});

	factory ImageStore.fromJson(Map<dynamic, dynamic> json) {
		return ImageStore(
			id: json['id'],
			name: json['name'],
			uniqueName: json['unique_name'],
			path: json['path'],
			bucketAmazon: json['bucket_amazon'],
			active: json['active'],
			userId: json['user_id'],
			fileTypeId: json['file_type_id'],
			url: json['url'],
			urlThumb: json['url_thumb'],
		);
	}
}