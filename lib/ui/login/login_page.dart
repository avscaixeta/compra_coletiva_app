import 'dart:convert';

import 'package:compra_coletiva/domain/services/login_service.dart';
import 'package:compra_coletiva/domain/services/store_service.dart';
import 'package:compra_coletiva/domain/services/secure_service.dart';
import 'package:compra_coletiva/ui/home/home_page.dart';
import 'package:compra_coletiva/utils/alerts.dart';
import 'package:compra_coletiva/utils/nav.dart';
import 'package:flutter/material.dart';

class _SecItem {
	final String key;
	final String value;

	_SecItem(this.key, this.value);
}

class LoginPage extends StatefulWidget {
	@override
	_LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
	final _tLogin = TextEditingController(text: "andrevini@gmail.com");
	final _tSenha = TextEditingController(text: "abcd1234");
	final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
	var _progress = false;

	@override
	Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
				backgroundColor: Color(0xFF333366),
				title: Text("SideBuySide - Compras Coletivas"),
			),
			body: Padding(
				padding: EdgeInsets.all(16),
				child: _body(context),
			),
		);
	}

	_body(context) {
		return Form(
			key: _formKey,
			child: ListView(
				children: <Widget>[
					_textFormField("Login",
						"Digite o login", false, _tLogin, _validateLogin, ),
					_textFormField(
						"Senha", "Digite a senha", true, _tSenha, _validateSenha),
					Container(
						height: 50,
						margin: EdgeInsets.only(top: 20),
						child: RaisedButton(
							color: Color(0xFF333366),
							child: _progress
								? CircularProgressIndicator(
								valueColor: AlwaysStoppedAnimation(Colors.white),
							)
								: Text(
								"Login",
								style: TextStyle(color: Colors.white, fontSize: 20),
							),
							onPressed: () {
								_onClickLogin(context);
							}),
					)
				],
			),
		);
	}

	TextFormField _textFormField(
		textParam, hintParam, obscureTextParam, controllerParam, validateParam) {
		return TextFormField(
			controller: controllerParam,
			validator: validateParam,
			//Serve para recuperar o valor digitado na tela (MODEL)
			obscureText: obscureTextParam,
			keyboardType: TextInputType.text,
			decoration: InputDecoration(
				labelText: textParam,
				labelStyle: TextStyle(color: Colors.blue, fontSize: 18),
				hintText: hintParam,
				hintStyle: TextStyle(
					color: Colors.black45,
					fontSize: 12,
				),
			),
		);
	}

	_onClickLogin(context) async {
		final login = _tLogin.text;
		final senha = _tSenha.text;

		//Primeira forma de validação
		if (!_formKey.currentState.validate()) {
			return null;
		}

		setState(() {
			_progress = true;
		});

		final response = await LoginService.login(login, senha);

		if (response.access_token != null) {
			SecureService.addNewItem(response.access_token);
			final storeResponse = await StoreService.getStore();
//			pushReplacement(context, HomePage(storeResponse));
		} else {
			alert(context, "ERRO", "Login ou senha inválidos!");
		}

		setState(() {
			_progress = false;
		});
	}

	String _validateLogin(String text) {
		if (text.isEmpty) {
			return "Informe o Login";
		}
	}

	String _validateSenha(String text) {
		if (text.isEmpty) {
			return "Informe a Senha";
		}

		if (text.length < 3) {
			return "A senha deve possuir pelo menos 3 caracateres.";
		}
	}
}
