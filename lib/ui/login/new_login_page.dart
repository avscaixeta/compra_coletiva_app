import 'package:compra_coletiva/domain/services/login_service.dart';
import 'package:compra_coletiva/domain/services/order_service.dart';
import 'package:compra_coletiva/domain/services/secure_service.dart';
import 'package:compra_coletiva/domain/services/store_service.dart';
import 'package:compra_coletiva/ui/home/home_page.dart';
import 'package:compra_coletiva/utils/alerts.dart';
import 'package:compra_coletiva/utils/nav.dart';
import 'package:flutter/material.dart';

class NewLoginPage extends StatefulWidget {
	NewLoginPage({Key key, this.title}) : super(key: key);

	final String title;

	@override
	_NewLoginPageState createState() => _NewLoginPageState();
}

class _NewLoginPageState extends State<NewLoginPage> {
	TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

	final _tLogin = TextEditingController(text: "andrevini@gmail.com");
	final _tSenha = TextEditingController(text: "abcd1234");

	final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
	var _progress = false;

	@override
	Widget build(BuildContext context) {
		final emailField = TextField(
			obscureText: false,
			style: style,
			controller: _tLogin,
			decoration: InputDecoration(
				contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
				hintText: "Email",
				border:
				OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
		);

		final passwordField = TextField(
			obscureText: true,
			style: style,
			controller: _tSenha,
			decoration: InputDecoration(
				contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
				hintText: "Password",
				border:
				OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
		);

		_onClickLogin(context) async {
			final login = _tLogin.text;
			final senha = _tSenha.text;

			//Primeira forma de validação
			if (!_formKey.currentState.validate()) {
				return null;
			}

			setState(() {
				_progress = true;
			});

			final response = await LoginService.login(login, senha);

			if (response.access_token != null) {
				SecureService.addNewItem(response.access_token);
				pushReplacement(context, HomePage(await StoreService.getStore()));
			} else {
				alert(context, "ERRO", "Login ou senha inválidos!");
			}

			setState(() {
				_progress = false;
			});
		}

		final loginButon = Material(
			elevation: 5.0,
			borderRadius: BorderRadius.circular(30.0),
			color: new Color(0xFF333366),
			child: MaterialButton(
				minWidth: MediaQuery.of(context).size.width,
				padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
				onPressed: () {
					_onClickLogin(context);
				},
				child: _progress
					? CircularProgressIndicator(
					valueColor: AlwaysStoppedAnimation(Colors.white),
				)
					: Text("Login",
					textAlign: TextAlign.center,
					style: style.copyWith(
						color: Colors.white, fontWeight: FontWeight.bold)),
			),
		);

		String _validateLogin(String text) {
			if (text.isEmpty) {
				return "Informe o Login";
			}
		}

		String _validateSenha(String text) {
			if (text.isEmpty) {
				return "Informe a Senha";
			}

			if (text.length < 3) {
				return "A senha deve possuir pelo menos 3 caracateres.";
			}
		}

		return Scaffold(
			body: Center(
				child: SingleChildScrollView(
					child: Container(
						color: Colors.white,
						child: Padding(
							padding: const EdgeInsets.all(36.0),
							child: Form(
								key: _formKey,
								child: Column(
									crossAxisAlignment: CrossAxisAlignment.center,
									mainAxisAlignment: MainAxisAlignment.center,
									children: <Widget>[
										SizedBox(
											height: 145.0,
											child: Image.asset(
												"assets/img/logo_sbs.png",
												fit: BoxFit.contain,
											),
										),
										SizedBox(height: 25.0),
										Padding(
											padding: const EdgeInsets.only(bottom: 20),
											child: Text(
												"O seu gerenciador de compras coletivas!",
												style:
												TextStyle(fontSize: 14.0, color: Color(0xFF333366)),
											),
										),
										SizedBox(height: 25.0),
										emailField,
										SizedBox(height: 25.0),
										passwordField,
										SizedBox(
											height: 35.0,
										),
										loginButon,
//									SizedBox(
//										height: 15.0,
//									),
									],
								),
							),
						),
					),
				),
			),
		);
	}
}
