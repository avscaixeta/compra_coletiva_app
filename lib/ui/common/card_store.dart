import 'package:compra_coletiva/domain/services/order_service.dart';
import 'package:compra_coletiva/domain/store.dart';
import 'package:flutter/material.dart';
import 'package:compra_coletiva/model/planets.dart';
import 'package:compra_coletiva/ui/common/separator.dart';
import 'package:compra_coletiva/ui/order/order_page.dart';
import 'package:compra_coletiva/ui/text_style.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class CardStore extends StatelessWidget {
	final Planet planet;
	final bool horizontal;
	final StoreData storeData;

	double cardHeightHoriz = 124;
	double cardHeightVerti = 144;

	CardStore(this.storeData, this.planet, this.cardHeightHoriz, this.cardHeightVerti, {this.horizontal = true});
	CardStore.vertical(this.storeData, this.planet) : horizontal = false;

	@override
	Widget build(BuildContext context) {

		final planetThumbnail = new Container(
			margin: new EdgeInsets.symmetric(vertical: 16.0),
			alignment:
			horizontal ? FractionalOffset.centerLeft : FractionalOffset.center,
			child: new Hero(
				tag: "planet-hero-${planet.id}",
				child:

				ClipRRect(
					borderRadius: new BorderRadius.circular(8.0),
					child:
					Image(
						image: new NetworkImage(storeData.imageStore.url),
						height: 92.0,
						width: 92.0,
					),
				)
			),
		);

		Widget _planetValue({String value, String image}) {
			return new Container(
				child: new Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
					Image.asset(image, height: 12.0),
					Container(width: 8.0),
					Text(value, style: Style.smallTextStyle),
				]),
			);
		}

		final planetCardContent = new Container(
			margin: new EdgeInsets.fromLTRB(
				horizontal ? 76.0 : 16.0, horizontal ? 16.0 : 42.0, 2.0, 16.0),
			constraints: new BoxConstraints.expand(),
			child: new Column(
				crossAxisAlignment:
				horizontal ? CrossAxisAlignment.start : CrossAxisAlignment.center,
				children: <Widget>[
					Container(
						height: 4.0,
					),
					Text(storeData.name, style: Style.titleTextStyle),
					Container(
						height: 10.0,
					),
					Text(storeData.user.name, style: Style.commonTextStyle, overflow: TextOverflow.clip, maxLines: 1,),
					Separator(),
					Row(
						mainAxisAlignment: MainAxisAlignment.center,
						children: <Widget>[
							new Expanded(
								flex: horizontal ? 1 : 0,
								child: _planetValue(
									value: MaskedTextController(mask: '(00)00000-0000', text: storeData.user.cellphone).text,
									image: 'assets/img/whatsapp.png')),
							new Container(
								width: horizontal ? 8.0 : 32.0,
							),
						],
					),
				],
			),
		);

		final planetCard = new Container(
			child: planetCardContent,
			height: horizontal ? cardHeightHoriz : cardHeightVerti,
			margin: horizontal
				? new EdgeInsets.only(left: 46.0)
				: new EdgeInsets.only(top: 72.0),
			decoration: new BoxDecoration(
				color: new Color(0xFF333366),
				shape: BoxShape.rectangle,
				borderRadius: new BorderRadius.circular(8.0),
				boxShadow: <BoxShadow>[
					new BoxShadow(
						color: Colors.black12,
						blurRadius: 10.0,
						offset: new Offset(0.0, 10.0),
					),
				],
			),
		);

		return new GestureDetector(
			onTap: () => _onCLickCard(context),
			child: new Container(
				margin: const EdgeInsets.symmetric(
					vertical: 16.0,
					horizontal: 24.0,
				),
				child: new Stack(
					children: <Widget>[
						planetCard,
						planetThumbnail,
					],
				),
			));
	}

	_onCLickCard(context) async {


		final orderResponse = await OrderService.getOrder();

		Navigator.of(context).push(
			new PageRouteBuilder(
				pageBuilder: (context, animation, secondaryAnimation) => OrderPage(planet, storeData, orderResponse),
				transitionsBuilder: (context, animation, secondaryAnimation,
					child) => FadeTransition(opacity: animation, child: child),
			),
		);
	}
}
