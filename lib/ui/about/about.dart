import 'package:flutter/material.dart';

class About extends StatelessWidget {
	static String tag = 'home-page';

	@override
	Widget build(BuildContext context) {

		var text = new RichText(
			text: new TextSpan(
				// Note: Styles for TextSpans must be explicitly defined.
				// Child text spans will inherit styles from parent
				style: new TextStyle(
					fontSize: 14.0,
					color: Colors.black,
				),
				children: <TextSpan>[
					new TextSpan(text: 'Desenvolvido por ', style: new TextStyle(fontSize: 12.0, color: Color(0xFF333366),)),
					new TextSpan(text: 'StarApp - Soluções em TI', style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 13.0, color: Color(0xFF333366),)),
				],
			),
		);

		return Center(
			child: Container(
				child: Column(
					children: <Widget>[
//						Hero(
//							tag: 'hero',
//							child: Padding(
//								padding: EdgeInsets.all(16.0),
//								child: CircleAvatar(
//									radius: 55.0,
//									backgroundColor: Colors.transparent,
//									backgroundImage: AssetImage('assets/img/logo_sbs.png',),
//
//								),
//							),
//						),


						Container(
						  width: 150.0,
						  height: 150.0,
						  child: ClipOval(
						  	child: Image.asset(
						  		"assets/img/logo_sbs.png",
						  		fit: BoxFit.cover,
						  		width: 150.0,
						  		height: 150.0,
						  	)
						  ),
						),

						Padding(
							padding: EdgeInsets.all(8.0),
							child: Text(
								'SideBuySide',
								style: TextStyle(fontSize: 18.0, color: Color(0xFF333366),),
							),
						),

						Padding(
							padding: EdgeInsets.all(8.0),
							child: Text(
								'Seu gerenciador de compras coletivas!',
								style: TextStyle(fontSize: 18.0, color: Color(0xFF333366),),
							),
						),

						Padding(
							padding: EdgeInsets.all(8.0),
							child:text
						),

						Padding(
							padding: EdgeInsets.all(32.0),
							child: Text(
								'Todos os direitos reservados.',
								style: TextStyle(fontSize: 8.0, color: Color(0xFF333366),),
							),
						)

					],
				),
			),
		);
	}
}
