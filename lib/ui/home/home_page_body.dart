import 'package:compra_coletiva/domain/store.dart';
import 'package:flutter/material.dart';
import 'package:compra_coletiva/model/planets.dart';
import 'package:compra_coletiva/ui/common/card_store.dart';

class HomePageBody extends StatefulWidget {

	final Store store;

	HomePageBody(this.store) : super();

  @override
  _HomePageBodyState createState() => _HomePageBodyState();
}

class _HomePageBodyState extends State<HomePageBody> {

	@override
	Widget build(BuildContext context) {
		return new Expanded(
			child: new Container(
				color: new Color(0xFF736AB7),
				child: new CustomScrollView(
					scrollDirection: Axis.vertical,
					shrinkWrap: false,
					slivers: <Widget>[
						new SliverPadding(
							padding: const EdgeInsets.symmetric(vertical: 24.0),
							sliver: new SliverList(
								delegate: new SliverChildBuilderDelegate(
										(context, index) => new CardStore(widget.store.data[index], planets[index], 124.0, 144.0),
									childCount: widget.store.data.length,
								),
							),
						),
					],
				),
			),
		);
	}
}
