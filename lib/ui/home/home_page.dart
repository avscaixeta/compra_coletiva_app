import 'package:compra_coletiva/domain/store.dart';
import 'package:compra_coletiva/domain/services/store_service.dart';
import 'package:compra_coletiva/ui/about/about.dart';
import 'package:compra_coletiva/ui/login/new_login_page.dart';
import 'package:compra_coletiva/utils/nav.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'home_page_body.dart';

class HomePage extends StatefulWidget {
	final Store store;

	HomePage(this.store);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
	int colorIcon = int.parse("#ff4081".substring(1, 7), radix: 16) + 0xFF000000;

	Store store;

	@override
	void initState() {
		store = widget.store;
	}

	final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
	new GlobalKey<RefreshIndicatorState>();

	Future<Null> _refresh() {

		return StoreService.getStore().then((_store) {
			setState(() => store = _store);
		});
	}

	@override
	Widget build(BuildContext context) {
		return new Scaffold(
			appBar: AppBar(
				title: Text(
					"Grupos que participo",
					style: TextStyle(color: Colors.white, fontSize: 18),
				),
				backgroundColor: new Color(0xFF333366),
				actions: [
					Padding(
						padding: const EdgeInsets.only(right: 6.0),
						child: Material(
							color: new Color(0xFF333366),
							child: Ink(
								decoration: BoxDecoration(
									border: Border.all(color: Colors.indigoAccent, width: 4.0),
									shape: BoxShape.circle,
								),
								child: InkWell(
									borderRadius: BorderRadius.circular(1000.0),
									onTap: () {
										_onClickInfo(context);
									},
									child: Padding(
										padding: EdgeInsets.all(12.0),
										child: Icon(
											FontAwesomeIcons.info,
											size: 15.0,
											color: Colors.white,
										),
									),
								),
							)),
					),

					Padding(
						padding: const EdgeInsets.only(right: 6.0),
						child: Material(
							color: new Color(0xFF333366),
							child: Ink(
								decoration: BoxDecoration(
									border: Border.all(color: Colors.indigoAccent, width: 4.0),
//									color: Colors.indigo[900],
									shape: BoxShape.circle,
								),
								child: InkWell(
									borderRadius: BorderRadius.circular(1000.0),
									onTap: () {
										_onClickSignOut(context);
									},
									child: Padding(
										padding: EdgeInsets.all(12.0),
										child: Icon(
											FontAwesomeIcons.signOutAlt,
											size: 15.0,
											color: Colors.white,
										),
									),
								),
							)),
					),
				],
			),
			body: RefreshIndicator(
				key: _refreshIndicatorKey,
				onRefresh: _refresh,
			child:
				Column(
					children: <Widget>[
						new HomePageBody(widget.store),
					],
				),
			),
		);
	}

	_onClickInfo(context) {
		showModalBottomSheet(
			isScrollControlled:true,
			context: context,
			builder: (BuildContext bc){
				return Container(
					height: 320.0,
					child: new Wrap(
						children: <Widget>[
							About(),
						],
					),
				);
			}
		);
	}

	_onClickSignOut(context) {
		pushReplacement(context, NewLoginPage());
	}
}

class GradientAppBar extends StatelessWidget {
	final String title;
	final double barHeight = 66.0;

	GradientAppBar(this.title);

	@override
	Widget build(BuildContext context) {
		final double statusBarHeight = MediaQuery.of(context).padding.top;

		return new Container(
			padding: new EdgeInsets.only(top: statusBarHeight),
			height: statusBarHeight + barHeight,
			child: new Center(
				child: new Text(
					title,
					style: const TextStyle(
						color: Colors.white,
						fontFamily: 'Poppins',
						fontWeight: FontWeight.w600,
						fontSize: 36.0),
				),
			),
			decoration: new BoxDecoration(
				gradient: new LinearGradient(
					colors: [const Color(0xFF3366FF), const Color(0xFF00CCFF)],
					begin: const FractionalOffset(0.0, 0.0),
					end: const FractionalOffset(1.0, 0.0),
					stops: [0.0, 1.0],
					tileMode: TileMode.clamp),
			),
		);
	}

}
