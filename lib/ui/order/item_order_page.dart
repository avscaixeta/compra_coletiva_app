
import 'package:compra_coletiva/domain/services/itens_pedido_service.dart';
import 'package:compra_coletiva/ui/common/separator.dart';
import 'package:compra_coletiva/ui/text_style.dart';
import 'package:flutter/material.dart';
import 'dart:math';

class ItemOrderPage extends StatefulWidget {
	ItemOrderPage() : super();

	final String title = "Lista de Itens Pedidos";

	@override
	ItemOrderPageState createState() => ItemOrderPageState();
}

class ItemOrderPageState extends State<ItemOrderPage> {
	//
//	List<String> companies;
	List<String> itensPedidoNome;
	var itensPedido = ItensPedidoService.getItensPedido();
	GlobalKey<RefreshIndicatorState> refreshKey;
	Random r;

	@override
	void initState() {
		super.initState();
		refreshKey = GlobalKey<RefreshIndicatorState>();
		r = Random();
	}

	addRandomCompany() {
		int nextCount = r.nextInt(100);
		setState(() {
			itensPedido = ItensPedidoService.getItensPedido();
		});
	}

	removeCompany(index) {
		setState(() {
			itensPedido.removeAt(index);
		});
	}

	undoDelete(index, company) {
		print("DELETANDO...");
		print(index);
		print(company);
		setState(() {
			itensPedido.insert(index, company);
		});
	}

	Future<Null> refreshList() async {
		await Future.delayed(Duration(seconds: 10));
		addRandomCompany();
		return null;
	}

	showSnackBar(context, company, index) {
		Scaffold.of(context).showSnackBar(SnackBar(
			content: Text('$company deleted'),
			action: SnackBarAction(
				label: "UNDO",
				onPressed: () {
					undoDelete(index, company);
				},

			),
		));
	}

	Widget refreshBg() {
		return Container(
			alignment: Alignment.centerRight,
			padding: EdgeInsets.only(right: 20.0),
			color: Colors.red,
			child: const Icon(
				Icons.delete,
				color: Colors.white,
			),
		);
	}

	Widget list() {

		return ListView.builder(
			physics: AlwaysScrollableScrollPhysics(), ///
			shrinkWrap: true, ///
			scrollDirection: Axis.vertical, ///
			padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0, top: 70.0),
			itemCount: itensPedido.length,
			itemBuilder: (BuildContext context, int index) {
				return GestureDetector(
					onTap: () =>_onClickCard(),
					child: Card(
						margin: EdgeInsets.only(right: 4.0, top: 4.0, left: 4.0, bottom: 4.0),
						elevation: 5,
						shape: RoundedRectangleBorder(
							borderRadius: BorderRadius.circular(8.0),
						),
						child: row(context, index)
					),
				);
			},
		);
	}

	_onClickCard() {
		print("CLICOUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU 123");
		showModalBottomSheet(
			isScrollControlled:true,
			context: context,
			builder: (BuildContext bc){
				return Container(
					height: 320.0,
					child: new Wrap(
						children: <Widget>[
							_form(),
						],
					),
				);
			}
		);
	}



	_form() {

		final screenSize = MediaQuery.of(context).size;

		return Form(
			child: Padding(
				padding: const EdgeInsets.all(16.0),
				child: Column(
					children: <Widget>[
						Padding(
							padding: const EdgeInsets.fromLTRB(0.0, 0.0, 64.0, 8.0),
							child: TextFormField(
								decoration: InputDecoration(
									icon: Icon(Icons.looks_one, color: new Color(0xFF333366),),
									labelText: "Quantidade",
								),
							),
						),
						Padding(
							padding: const EdgeInsets.fromLTRB(0.0, 0.0, 64.0, 8.0),
							child: TextFormField(
								decoration: InputDecoration(
									icon: Icon(Icons.looks_two, color: new Color(0xFF333366),),
									labelText: "Tamanho",
								),
							),
						),
						Padding(
							padding: const EdgeInsets.fromLTRB(0.0, 0.0, 64.0, 8.0),
							child: TextFormField(
								decoration: InputDecoration(
									icon: Icon(Icons.looks_3, color: new Color(0xFF333366),),
									labelText: "Cor",
								),
							),
						),



						new Container(
							width: screenSize.width,
							padding: EdgeInsets.only(top: 20.0),
							child: new Material(
								color: Colors.teal,
								elevation: 3.0,
								child: new MaterialButton(
									height: 50.0,
									child: new Text(
										'Salvar Item',
										style: new TextStyle(
											color: Colors.white
										),
									),
									onPressed: () => null,
									color: new Color(0xFF333366),
								),
							)
						),


					],
				),
			),
		);

	}

	Widget row(context, index) {

		return Dismissible(
			key: Key(itensPedido[index].cod.toString()), // UniqueKey().toString()
			onDismissed: (direction) {
				var company = itensPedido[index];
				showSnackBar(context, company, index);
				removeCompany(index);
			},
			background: refreshBg(),
			child:

			ClipRRect(
				borderRadius: BorderRadius.circular(8.0),
			  child: Container(
			  	color: new Color(0xFF333366),
			  	child: Container(
			  		width: 350.0,
			  	  	height: 90.0,
			  	  	child: Column(
			  			children: <Widget>[
			  				Padding(
			  				  padding: const EdgeInsets.only(top: 4.0),
			  				  child: Row(
			  				  	mainAxisAlignment: MainAxisAlignment.center,
			  				  	children: <Widget>[
			  				  		new Expanded(
			  				  			flex: 0,
			  				  			child: Container(child: Text(itensPedido[index].nome, style: TextStyle(color: Colors.white),),)
			  				  		),
			  				  		new Container(
			  				  			width: 32.0,
			  				  		),
			  				  	],
			  				  ),
			  				),
			  				Separator(),
			  				Padding(
			  				  padding: const EdgeInsets.only(left: 8.0),
			  				  child: Row(
			  				  	mainAxisAlignment: MainAxisAlignment.spaceEvenly,
			  				  	children: <Widget>[
			  				  		new Expanded(
			  				  			flex: 0,
			  				  			child: Container(child: Text("Quantidade: ",  style: Style.commonTextStyle),)
			  				  		),
			  				  		new Expanded(
			  				  			flex: 1,
			  				  			child: Container(child: Text(itensPedido[index].qtde.toString(),  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.white),),)
			  				  		),


			  				  	],
			  				  ),
			  				),
						    Container(
								margin: new EdgeInsets.symmetric(vertical: 4.0),
						    ),
			  				Padding(
			  					padding: const EdgeInsets.only(left: 8.0),
			  					child: Row(
			  						mainAxisAlignment: MainAxisAlignment.spaceEvenly,
			  						children: <Widget>[

			  							new Expanded(
			  								flex: 0,
			  								child: Container(child: Text("Tamanho: ",  style: Style.commonTextStyle),)
			  							),
			  							new Expanded(
			  								flex: 1,
			  								child: Container(child: Text(itensPedido[index].tam.toString(),  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.white),),)
			  							),
			  							new Expanded(
			  								flex: 0,
			  								child: Container(child: Text("Cor: ", style: Style.commonTextStyle),)
			  							),
			  							new Expanded(
			  								flex: 1,
			  								child: Container(child: Text(itensPedido[index].cor,  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.white),),)
			  							),
			  						],
			  					),
			  				),
			  			],
			  	  ),
			  	),
			  ),
			),
		);
	}

	@override
	Widget build(BuildContext context) {
		return Scaffold(

			appBar: AppBar(
				title: Text(
					"Itens do Pedido",
					style: TextStyle(color: Colors.white, fontSize: 18),
				),
				backgroundColor: new Color(0xFF333366),
			),

			body: RefreshIndicator(
				key: refreshKey,
				onRefresh: () async {
					await refreshList();
				},
				child:
					Container(
						constraints: new BoxConstraints.expand(),
						color: new Color(0xFF736AB7),
						child: Stack(
							children: <Widget>[
								_getBackground(),
								_getGradient(),
								list(),
//								_getToolbar(context),
							],
						)
					),
			),
		);
	}

	Container _getBackground () {
		return new Container(
			child: new Image.network("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSxgqVR1vH4sRyE4Hu3g5PoKc4jWe-8qypO6Kb3xkofz4yPFTd",
				fit: BoxFit.cover,
				height: 300.0,
			),
			constraints: new BoxConstraints.expand(height: 295.0),
		);
	}

	Container _getGradient() {
		return new Container(
			margin: new EdgeInsets.only(top: 190.0),
			height: 110.0,
			decoration: new BoxDecoration(
				gradient: new LinearGradient(
					colors: <Color>[
						new Color(0x00736AB7),
						new Color(0xFF736AB7)
					],
					stops: [0.0, 0.9],
					begin: const FractionalOffset(0.0, 0.0),
					end: const FractionalOffset(0.0, 1.0),
				),
			),
		);
	}

	Container _getToolbar(BuildContext context) {
		return new Container(
			margin: new EdgeInsets.only(
				top: MediaQuery
					.of(context)
					.padding
					.top),
			child: new BackButton(color: Colors.white),
		);
	}
}