import 'package:compra_coletiva/domain/order.dart';
import 'package:compra_coletiva/model/planets.dart';
import 'package:compra_coletiva/ui/order/item_order_page.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class OrderListPage extends StatelessWidget {
	final Planet planet;
	final Order order;

	OrderListPage(this.planet, this.order);

	@override
	Widget build(BuildContext context) {
		var _width = MediaQuery.of(context).size.width / 2;

		void _onCardClicked(int index) {
			debugPrint("You tapped on item $index");
			Navigator.of(context).push(
				new PageRouteBuilder(
					pageBuilder: (_, __, ___) => ItemOrderPage(),
					transitionsBuilder: (context, animation, secondaryAnimation, child) =>
					new FadeTransition(opacity: animation, child: child),
				),
			);
		}

		return Container(
			margin: const EdgeInsets.symmetric(
				vertical: 12.0,
				horizontal: 0.0,
			),
			child: SingleChildScrollView(
				physics: BouncingScrollPhysics(),
				child: Column(
					children: <Widget>[
						ListView.builder(
							shrinkWrap: true,
							physics: BouncingScrollPhysics(),
							itemCount: order.orderItems.orderData.length,
							itemBuilder: (context, index) {
								return Padding(
									padding: const EdgeInsets.only(
										right: 4.0, top: 0.0, left: 4.0, bottom: 12.0),
									child: Column(
										children: <Widget>[
											Card(
												margin: EdgeInsets.only(
													right: 4.0, top: 2.0, left: 4.0, bottom: 0.0),
												elevation: 5,
												shape: RoundedRectangleBorder(
													borderRadius: BorderRadius.circular(8.0),
												),
												child: InkWell(
													onTap: () => _onCardClicked(0),
													child: Container(
														width: 400.0,
														height: _width - 72,
														child: ClipRRect(
															borderRadius: BorderRadius.circular(8.0),
															child: Container(
																color: new Color(0xFF333366),
																child: Padding(
																	padding: const EdgeInsets.only(
																		top: 8.0, right: 4.0, left: 0.0),
																	child: _contentCardOrder(index),
																),
															),
														),
													),
												),
											),
										],
									),
								);
							},
						)
					],
				),
			),
		);
	}

	_contentCardOrder(index) {
		final formatter = new DateFormat('dd/MM/yyyy');
		DateTime todayDate = DateTime.parse(
			order.orderItems.orderData[index].orderHistory.statusLimitDate);
		String statusLimitDate = formatter.format(todayDate);

		return Column(
			crossAxisAlignment: CrossAxisAlignment.center,
			children: <Widget>[
				Text(
					order.orderItems.orderData[index].name,
					textAlign: TextAlign.center,
					style: TextStyle(fontFamily: 'Poppins').copyWith(
						color: Colors.white, fontSize: 11.0, fontWeight: FontWeight.w600),
				),
				Container(
					height: 16.0,
				),
				Row(
					children: <Widget>[
						Padding(
							padding: const EdgeInsets.only(left: 10.0),
							child: Text(
								"Situação do Pedido:   ",
								textAlign: TextAlign.center,
								style: TextStyle(fontFamily: 'Poppins').copyWith(
									color: const Color(0xffb6b2df),
									fontSize: 12.0,
									fontWeight: FontWeight.w600),
							),
						),
						Text(
							order.orderItems.orderData[index].orderHistory.statusOrderHistory
								.name,
							textAlign: TextAlign.center,
							style: TextStyle(fontFamily: 'Poppins').copyWith(
								color: Colors.white,
								fontSize: 12.0,
								fontWeight: FontWeight.w600),
						),
					],
				),
				Container(
					height: 8.0,
				),
				Row(
					children: <Widget>[
						Padding(
							padding: const EdgeInsets.only(left: 12.0),
							child: Text(
								"Data Inicial: ",
								textAlign: TextAlign.center,
								style: TextStyle(fontFamily: 'Poppins').copyWith(
									color: const Color(0xffb6b2df),
									fontSize: 12.0,
									fontWeight: FontWeight.w600),
							),
						),
						Padding(
							padding: const EdgeInsets.only(left: 48.0),
							child: Text(
								statusLimitDate,
								textAlign: TextAlign.center,
								style: TextStyle(fontFamily: 'Poppins').copyWith(
									color: Colors.white,
									fontSize: 12.0,
									fontWeight: FontWeight.w600),
							),
						),
					],
				),
				Container(
					height: 4.0,
				),
				Row(
					children: <Widget>[
						Padding(
							padding: const EdgeInsets.only(left: 13.0),
							child: Text(
								"Data Final: ",
								textAlign: TextAlign.center,
								style: TextStyle(fontFamily: 'Poppins').copyWith(
									color: const Color(0xffb6b2df),
									fontSize: 12.0,
									fontWeight: FontWeight.w600),
							),
						),
						Padding(
							padding: const EdgeInsets.only(left: 54.0),
							child: Text(
								statusLimitDate,
								textAlign: TextAlign.center,
								style: TextStyle(fontFamily: 'Poppins').copyWith(
									color: Colors.white,
									fontSize: 12.0,
									fontWeight: FontWeight.w600),
							),
						),
					],
				),
			],
		);
	}

	final planetThumbnail = new Container(
		margin: new EdgeInsets.symmetric(vertical: 16.0),
		alignment: FractionalOffset.center,
		child: new Hero(
			tag: "planet-hero",
			child: new Image(
				image: new AssetImage("assets/img/logout.png"),
				height: 92.0,
				width: 92.0,
			),
		),
	);
}
