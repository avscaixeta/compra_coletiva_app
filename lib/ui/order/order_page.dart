import 'package:compra_coletiva/domain/order.dart';
import 'package:compra_coletiva/domain/store.dart';
import 'package:compra_coletiva/ui/order/order_list_page.dart';
import 'package:flutter/material.dart';
import 'package:compra_coletiva/model/planets.dart';
import 'package:compra_coletiva/ui/common/card_store.dart';
import 'package:compra_coletiva/ui/common/separator.dart';
import 'package:compra_coletiva/ui/text_style.dart';

class OrderPage extends StatefulWidget {
	final Planet planet;
	final StoreData storeData;
	final Order order;

	OrderPage(this.planet, this.storeData, this.order);

	@override
	_OrderPageState createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> {
	@override
	Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
				title: Text(
					"Lista de Pedidos",
					style: TextStyle(color: Colors.white, fontSize: 18),
				),
				backgroundColor: new Color(0xFF333366),
			),
			body: _body());
	}

	_body() {
		return Container(
			constraints: new BoxConstraints.expand(),
			color: new Color(0xFF736AB7),
			child: new Stack(
				children: <Widget>[
					_getBackground(),
					_getGradient(),
					_getContent(),
				],
			),
		);
	}

	Container _getBackground() {
		return new Container(
			child: new Image.network(
				"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSxgqVR1vH4sRyE4Hu3g5PoKc4jWe-8qypO6Kb3xkofz4yPFTd",
				fit: BoxFit.cover,
				height: 300.0,
			),
			constraints: new BoxConstraints.expand(height: 295.0),
		);
	}

	Container _getGradient() {
		return new Container(
			margin: new EdgeInsets.only(top: 190.0),
			height: 110.0,
			decoration: new BoxDecoration(
				gradient: new LinearGradient(
					colors: <Color>[new Color(0x00736AB7), new Color(0xFF736AB7)],
					stops: [0.0, 0.9],
					begin: const FractionalOffset(0.0, 0.0),
					end: const FractionalOffset(0.0, 1.0),
				),
			),
		);
	}

	Container _getContent() {
		final _overviewTitle = "Lista de Pedidos".toUpperCase();
		return new Container(
			child: new ListView(
				children: <Widget>[
					new CardStore(
						widget.storeData,
						widget.planet,
						124.0,
						140.0,
						horizontal: false,
					),
					new Container(
						padding: new EdgeInsets.only(
							top: 16.0, left: 4.0, right: 4.0, bottom: 2.0),
						child: new Column(
							crossAxisAlignment: CrossAxisAlignment.start,
							children: <Widget>[
								new Text(
									_overviewTitle,
									style: Style.headerTextStyle,
								),
								new Separator(),
								OrderListPage(widget.planet, widget.order)
							],
						),
					),
				],
			),
		);
	}
}
